
//Steven Olsen
//CS 1415
//March 15, 2016
//Lab 11

/*
Functions of the circle class.
*/

#include "OlseStevCircle.h"
using namespace OlseStev;
#include <iostream>
using namespace std;

//Purpose: Constructor for the circle class. Also default constructor.
//Receives: Up to one double representing radius.
//Returns: Circle object.
OlseStev::Circle::Circle(double inRadius) : OlseStev::Shape::Shape()
{
	if (inRadius > 0) radius = inRadius;
	else radius = 1;
}


//Purpose: Calculates area using members.
//Returns: A double representing the area.
double OlseStev::Circle::getArea() const
{
	double temp;
	temp = PI * (radius * radius);
	return temp;
}

//Purpose: Calculates perimeter using members.
//Returns: A double representing the perimeter.
double OlseStev::Circle::getPerimeter() const
{
	double temp;
	temp = 2 * PI * radius;
	return temp;
}

//Purpose: Outputs the members of the object.
//Output: Name of object, area, perimeter, and radius.
void OlseStev::Circle::print() const
{
	cout << "Circle:" << endl;
	cout << "Radius: " << radius << endl;
	cout << "Area: " << getArea() << endl;
	cout << "Circumfrence: " << getPerimeter() << endl;
}

//Purpose: Sets radius member if a valid value.
//Receives: Double representing the radius.
void OlseStev::Circle::setRadius(double temp)
{
	if (temp > 0) {
		radius = temp;
	}
}

//Purpose: Returns the radius variable.
//Returns: double representing the radius.
double OlseStev::Circle::getRadius() const
{
	return radius;
}
