//Steven Olsen
//CS 1415
//March 15, 2016
//Lab 11

/*
Header file for Triangle class derived from Shape abstract base class.
*/

//Include guards.
#ifndef TRIANGLE_H

#define TRIANGLE_H

//Required library.
#include "OlseStevShape.h"
using namespace OlseStev;

//Namespace
namespace OlseStev {

	//Triangle class. Overrides print, getArea and getPerimeter functions, provides constructors.
	//Represents a triangle. Functions that change side lengths only accept valid values.
	class Triangle : public Shape {

	public:
		//Constructors.
		Triangle(double = 1, double = 1, double = 1);
		//Overriden functions.
		double getArea() const override;
		double getPerimeter() const override;
		void print() const override;
		//Functions to set side lengths.
		void setSingleSide(double, char);
		void setAllSides(double, double, double);
		double getSide(char) const;

	private:
		double sides[3]; //Array for the sides of the triangle.

	};

}
#endif