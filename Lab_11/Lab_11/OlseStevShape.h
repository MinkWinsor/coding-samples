//Steven Olsen
//CS 1415
//March 15, 2016
//Lab 11

/*
Header file for abstract shape class. Provides three pure  virtual functions.
*/

//Include guards.
#ifndef SHAPE_H

#define SHAPE_H

//Namespace
namespace OlseStev {

	//Shape class. Provides three pure virtual functions to be overriden in derived classes.
	class Shape {
	public:
		virtual double getArea() const = 0;
		virtual double getPerimeter() const = 0;
		virtual void print() const = 0;
	};

}

#endif
