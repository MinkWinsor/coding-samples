
//Steven Olsen
//CS 1415
//March 15, 2016
//Lab 11

/*
This file includes the function definitions of the triangle class.
*/

//Required Libraries.
#include <cmath>
#include <iostream>
using namespace std;
#include "OlseStevTriangle.h"
using namespace OlseStev;

//Purpose: Constructs an instance of the class using doubles.
//Receives: Three doubles for the triangle sides.
//Returns: The triangle object.
OlseStev::Triangle::Triangle(double a, double b, double c) : OlseStev::Shape::Shape()
{
	//Sides set if larger than zero. If side input is too small, changed to 1. Last side if too small is set to minimum possible value.
	if (a > 0) sides[0] = a;
	else sides[0] = 1;
	if (b > 0) sides[1] = b;
	else sides[1] = 1;
	if (c <= (sides[0] + sides[1]) && c > 0) sides[2] = c;
	else sides[2] = abs(sides[0] - sides[1]);
}


//Purpose: Calculates area using members.
//Returns: A double representing the area.
double OlseStev::Triangle::getArea() const
{
	//Temp variable created and used to find the area of the triangle. Square root function used.
	double temp;
	double areaTemp;
	temp = (sides[0] + sides[1] + sides[2]) / 2;
	areaTemp = sqrt(temp * (temp - sides[0]) * (temp - sides[1]) * (temp - sides[2]));
	return areaTemp;
}

//Purpose: Calculates perimeter using members.
//Returns: A double representing the perimeter.
double OlseStev::Triangle::getPerimeter() const
{
	return (sides[0] + sides[1] + sides[2]); //Calculated and returned.
}

//Purpose: Outputs the members of the object.
//Output: Name of object, area, perimeter, and side lengths.
void OlseStev::Triangle::print() const
{
	cout << "Triangle:\n";
	cout << "Side A: " << sides[0] << endl;
	cout << "Side B: " << sides[1] << endl;
	cout << "Side C: " << sides[2] << endl;
	cout << "Area: " << getArea() << endl;
	cout << "Perimeter: " << getPerimeter() << endl;
}

//Purpose: Sets side member if a valid value.
//Receives: Double representing the side length and character representing which side.
void OlseStev::Triangle::setSingleSide(double newLength, char sideNum)
{
	//Checks each input to see which side it is, makes sure the input length is large enough
	//To be a triangle side, if not, changes it to minimum possible size. If length is 0 or less, nothing happens.
	toupper(sideNum);
	if (sideNum = 'A') 
	{
		if (newLength >= (sides[1] + sides[2])) sides[0] = newLength;
		else sides[0] = (sides[1] + sides[2]);
	}
	if (sideNum = 'B') sides[1] = newLength;
	{
		if (newLength >= (sides[0] + sides[2])) sides[1] = newLength;
		else sides[1] = (sides[0] + sides[2]);
	}
	if (sideNum = 'C') sides[2] = newLength;
	{
		if (newLength >= (sides[1] + sides[0])) sides[2] = newLength;
		else sides[2] = (sides[1] + sides[0]);
	}
}

//Purpose: Sets side members if valid values.
//Receives: Doubles representing sides.
void OlseStev::Triangle::setAllSides(double a, double b, double c)
{
	//First two sides set.
	if (a > 0) sides[0] = a;
	else sides[0] = 1;
	if (b > 0) sides[1] = b;
	else sides[1] = 1;
	if (c <= (sides[0] + sides[1]) && c > 0) sides[2] = c;
	else sides[2] = abs(sides[0] - sides[1]);
}

//Purpose: Returns a side of the triangle.
//Receives: Character for one side of the triangle.
//Returns: That triangle side.
double OlseStev::Triangle::getSide(char sideNum) const
{
	toupper(sideNum);
	if (sideNum = 'A') return sides[0];
	if (sideNum = 'B') return sides[1];
	if (sideNum = 'C') return sides[2];
}
