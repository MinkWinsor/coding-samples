//Steven Olsen
//CS 1415
//March 15, 2016
//Lab 11

/*
Header file for Square class.
*/


//Include guards
#ifndef SQUARE_H

#define SQUARE_H

//Required library
#include "OlseStevRectangle.h"
using namespace OlseStev;

//Namespace
namespace OlseStev {

	//Square class derived from rectangle class. Uses rectangle class's length variable, if user attempts to access width it returns length instead.
	class Square : public Rectangle {

	public:
		//Constructors.
		Square(double = 1);
		//Overriden functions.
		double getArea() const override;
		double getPerimeter() const override;
		void print() const override;
		virtual double getLength() const override;
		virtual double getWidth() const override;
		virtual void setLength(double) override;

	};

}

#endif