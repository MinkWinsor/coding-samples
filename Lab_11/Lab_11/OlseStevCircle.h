//Steven Olsen
//CS 1415
//March 15, 2016
//Lab 11

/*
Header file for the Circle class.
*/

//Include guards
#ifndef CIRCLE_H

#define CIRCLE_H

//Required library to derive from shape class.
#include "OlseStevShape.h"
using namespace OlseStev;

//Namespace
namespace OlseStev {

	//Circle class, derived from Shape class. Overrides functions and contains radius.
	class Circle : public Shape {

	public:
		//Constructors
		Circle(double = 1);
		//Overriden functions
		double getArea() const override;
		double getPerimeter() const override;
		void print() const override;
		//Get and set functions.
		void setRadius(double);
		double getRadius() const;

	private:
		double radius; //Represents circle.
		const double PI = 3.14159265; //Used to calculate area and circumfrence.
	};

}
#endif