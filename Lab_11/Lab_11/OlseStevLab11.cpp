//Steven Olsen
//CS 1415
//March 15, 2016
//Lab 11

/*
Driver program to demonstrate the classes.
*/



#include <iostream>
using namespace std;
#include "OlseStevCircle.h"
#include "OlseStevRectangle.h"
#include "OlseStevSquare.h"
#include "OlseStevTriangle.h"
using namespace OlseStev;

//Function declarations.
void printShape(Shape &);

//Purpose: Driver program to demonstrate the classes.
//Output: Instructions to the user.
//Input: Shape dimensions from the user.
//Returns: 0 to signify the program is over.
int main() {
	//Holder variables for creating shapes.
	double input1, input2, input3;
	Shape *shapePointer = new Rectangle;

	cout << "Welcome to the program. This program demonstrates certain shape objects\n";
	cout << "Namely a circle, rectangle, square, and triangle\n";
	cout << "Please note invalid output is automatically changed to proper values.\n\n";

	//User inputs triangle sides through holder variables.
	cout << "Input triangle sides. Side A: ";
	cin >> input1;
	cout << "Input side B: ";
	cin >> input2;
	cout << "Input side C: ";
	cin >> input3;
	Triangle myTriangle(input1, input2, input3); //Triangle object created.

	//User inputs rectangle sides.
	cout << "Input rectangle length: ";
	cin >> input1;
	cout << "Input rectangle width: ";
	cin >> input2;
	Rectangle myRect(input1, input2); //Rectangle object created.

	//User inputs square length.
	cout << "Input square length: ";
	cin >> input1;
	Square mySquare(input1); //Square object created.

	//User inputs circle radius.
	cout << "Input circle radius: ";
	cin >> input1;
	Circle myCircle(input1); //Circle object created.

	cout << "Results of objects. Remember incorrect input has been modified.\n";

	//Shape pointers passed to printing function outside of main.
	shapePointer = &myTriangle;
	printShape(*shapePointer);
	shapePointer = &myRect;
	printShape(*shapePointer);
	shapePointer = &mySquare;
	printShape(*shapePointer);
	shapePointer = &myCircle;
	printShape(*shapePointer);

	return 0;
}

//Purpose: Calls print function of variables. This is unneccessary but required by the program requirements.
//Receives: Shape pointer.
void printShape(Shape &myShape) 
{
	Shape *shapePointer = &myShape;
	shapePointer->print();
}