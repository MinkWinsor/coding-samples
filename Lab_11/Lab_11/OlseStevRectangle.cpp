
//Steven Olsen
//CS 1415
//March 15, 2016
//Lab 11

/*

*/

#include <iostream>
using namespace std;
#include "OlseStevRectangle.h"
using namespace OlseStev;

//Purpose: Constructor for the rectangle class. Also default constructor.
//Receives: Up to two doubles for width and length
//Returns: Rectangle object.
OlseStev::Rectangle::Rectangle(double newLength, double newWidth) : OlseStev::Shape::Shape()
{
	if (newWidth > 0) width = newWidth;
	else width = 1;
	if (newLength > 0)length = newLength;
	else length = 1;
}

//Purpose: Calculates area using members.
//Returns: A double representing the area.
double OlseStev::Rectangle::getArea() const
{
	return (length * width);
}

//Purpose: Calculates perimeter using members.
//Returns: A double representing the perimeter.
double OlseStev::Rectangle::getPerimeter() const
{
	return (length * 2 + width * 2);
}

//Purpose: Outputs the members of the object.
//Output: Name of object, area, perimeter, and length and width.
void OlseStev::Rectangle::print() const
{
	cout << "Rectangle: " << endl;
	cout << "Length: " << length << endl;
	cout << "Width: " << width << endl;
	cout << "Area: " << getArea() << endl;
	cout << "Perimeter: " << getPerimeter() << endl;
}

//Purpose: Returns the length variable.
//Returns: double representing the length.
double OlseStev::Rectangle::getLength() const
{
	return length;
}

//Purpose: Returns the width variable.
//Returns: double representing the width.
double OlseStev::Rectangle::getWidth() const
{
	return width;
}

//Purpose: Sets length member if a valid value.
//Receives: Double representing the length.
void OlseStev::Rectangle::setLength(double temp)
{
	if (temp > 0) length = temp;
}

//Purpose: Sets width member if a valid value.
//Receives: Double representing the width.
void OlseStev::Rectangle::setWidth(double temp)
{
	if (temp > 0) width = temp;
}
