//Steven Olsen
//CS 1415
//March 15, 2016
//Lab 11

/*
Header file for the rectangle class. Represents a rectangle using length and width.
*/

//Include guards.
#ifndef RECTANGLE_H

#define RECTANGLE_H

//Required libraries.
#include "OlseStevShape.h"
using namespace OlseStev;

//Namespace
namespace OlseStev {

	//Rectangle class, derived from abstract shape class.
	class Rectangle : public Shape {

	public:
		//Constructors.
		Rectangle(double = 1, double = 1);
		//Overriden functions.
		double getArea() const override;
		double getPerimeter() const override;
		void print() const override;
		//Get and set functions for width and length
		virtual double getLength() const;
		virtual double getWidth() const;
		virtual void setLength(double);
		void setWidth(double);
		//Length variable, can be accessed by derived Square class.
	protected:
		double length;
		//Width variable, kept private.
	private:
		double width;
	};

}

#endif