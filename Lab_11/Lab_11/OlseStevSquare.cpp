
//Steven Olsen
//CS 1415
//March 15, 2016
//Lab 11

/*

*/

#include <iostream>
using namespace std;
#include "OlseStevSquare.h"
using namespace OlseStev;

//Purpose: Constructor for the square class. Also default constructor.
//Receives: Up to one double representing length.
//Returns: Square object.
OlseStev::Square::Square(double newLength) : Rectangle (newLength, newLength)
{
	if (newLength > 0) length = newLength;
	else length = 1;
}


//Purpose: Calculates area using members.
//Returns: A double representing the area.
double OlseStev::Square::getArea() const
{
	return (length * length);
}

//Purpose: Calculates perimeter using members.
//Returns: A double representing the perimeter.
double OlseStev::Square::getPerimeter() const
{
	return (length * 4);
}

//Purpose: Outputs the members of the object.
//Output: Name of object, area, perimeter, and length.
void OlseStev::Square::print() const
{
	cout << "Square: " << endl;
	cout << "Length: " << length << endl;
	cout << "Area: " << getArea() << endl;
	cout << "Perimeter: " << getPerimeter() << endl;
}

//Purpose: Returns the length variable.
//Returns: double representing the length.
double OlseStev::Square::getLength() const
{
	return length;
}

//Purpose: Returns the length variable, even though this function shouldn't be called from a square class.
//Returns: double representing the length.
double OlseStev::Square::getWidth() const
{
	return length;
}

//Purpose: Sets length member if a valid value.
//Receives: Double representing the length.
void OlseStev::Square::setLength(double temp)
{
	if (temp > 0) length = temp;
}
