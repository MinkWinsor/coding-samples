//Steven Olsen
//CS 1415
//March 23, 2016
//Lab 12

/*
This program shows the different exception classes that can now be thrown. 
It uses a menu style  approach to allow the user to select a function they want to run to throw a certain exception.
*/

//Required libraries
#include <iostream>
using namespace std;
#include "OlseStevExceptions.h"
using namespace OlseStev;

//Function declarations for functions that throw exceptions.
void overflowMaker(short);
void findArray();
void failMemory();


//Purpose: Driver program.
//Inputs: Selection from user on which exception to try and throw.
//Outputs: Instructions to the user.
//Returns: 0 To signify the program is over.
int main() {
	//Bool for looping the menu and int for user selection.
	bool tryAgain = true;
	int userInput;

	//Menu will show after each function until user quits program.
	while (tryAgain == true)
	{

		//The main try block. Catches exceptions from the possible functions, and uses classes listed in OlseStevExceptions.h to pass data to catch blocks.
		try
		{

			//User given a menu of exceptions that they can attempt.
			cout << "Welcome to the program. Which exception would you like to throw?" << endl;
			cout << "Type in selection:" << endl;
			cout << "1: Overflow Exception." << endl;
			cout << "2: Subscript Exception (Out of bounds array)." << endl;
			cout << "3: Memory Exception." << endl;
			cout << "4: Quit Program." << endl;
			if (isdigit(cin.peek())) {//Input validated as a digit.
				cin >> userInput;
				cin.ignore();
				switch (userInput) {
				case 1: //first selection calls overflow function to throw an exception.
					overflowMaker(32700);
					break;
				case 2: //Function called to throw subscript exception.
					findArray();
					break;
				case 3: //Function called to overload computer memory.
					failMemory();
					break;
				case 4: //Program ends.
					cout << "Goodbye!" << endl;
					tryAgain = false;
					break;
				default : //User tries again if selection is invalid.
					cout << "Not a valid number. Try again." << endl;
				}

			}
			else { //If not a number, input stream cleared and user asked to try again.
				cout << "Invalid input. Must be a number." << endl;
				cin.ignore(2);
			}


		} //End of try block.


		//Catch blocks for each possible exception.
		catch (OverflowException except) //Data from Overflow Exception classs displayed.
		{
			cout << except.what();
			cout << except.getNum();
			cout << endl << endl;
		}

		catch (SubscriptException except) //Data from Subscript Exception classs displayed.
		{
			cout << except.what();
			cout << except.getSub();
			cout << endl << endl;
		}

		catch (MemoryException except) //Data from Memory Exception classs displayed.
		{
			cout << except.what();
			cout << except.getSize();
			cout << endl << endl;
		}

	} //END OF WHILE LOOP
	return 0;
}

//Purpose: Create an overflow exception, where a variable can't hold a number of a certain size.
//Receives: A number to start  counting at.
//Throws: OverflowException class.
void overflowMaker(short bigNum) {
	short testNum = bigNum;

	for ( ; ; ) { //INFINITE LOOP
		
		//Number added to until it goes negative. When it does, the last biggest number is passed to an OverflowException class which is thrown.
		bigNum++;
		if (bigNum <= testNum) {
			bigNum--;
			throw OverflowException(bigNum);
		}
		else
			cout << "Number at: " << bigNum << endl;
			testNum++;
	}		

}



//Purpose: Create a subscript exception, where the program steps outside the bounds of an array.
//Throws: SubscriptException class.
void findArray() {
	//Array made.
	int myArray[25];
	int arraySize = (sizeof(myArray) / sizeof(int)); //Size of array found.
	//Infinite loop. Terminates when SubscriptException class is thrown with size of array at end.
	for (int count = 0; count >= 0; count++) {
		if (count < arraySize) {
			cout << "Subscript at: " << count << endl;
		}
		else
			throw SubscriptException(count);
	}
	
}

//Purpose: Create a Memory exception, where the computer can't hold an array of a certain size.
//Throws: MemoryException class.
void failMemory() {

	//Array size variable and array pointer.
	long arraySize = 1;
	int *bigArray = nullptr;

	//Bigger and bigger arrays allocated until bad_alloc is thrown.
	try {
		for ( ; ; ) { //Unending loop.

			bigArray = new int[arraySize];
			delete[] bigArray;
			arraySize = arraySize * 10; //Size multiplied by ten each time.

		}

	}
	catch(bad_alloc){ //When bad_alloc is thrown, a MemoryException class containing the size the computer couldn't handle is created and thrown.
		throw MemoryException(arraySize);
	}

}