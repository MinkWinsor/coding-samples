//Steven Olsen
//CS 1415
//March 23, 2016
//Lab 12

/*
Exception classes for Overflow, subscript, and memory exceptions.
*/

//Required Libraries.
#include <exception>
using namespace std;



//Namespace.
namespace OlseStev 
{

	//Include guards.
#ifndef OLSESTEV_EXCEPTION

#define OLSESTEV_EXCEPTION


	//Overflow Exception class. Derived from exception class.
	//Contains constructor, message to pass to exception class for 'what' function, get function for local value exception was thrown at.
	class OverflowException : public exception {
	public:
		OverflowException(int newNum) : exception("Overflow Exception thrown. Number at: ") { overflowNum = newNum; }
		int getNum() { return overflowNum; }
	private:
		int overflowNum;
	};

	//Subscript Exception class. Derived from exception class.
	//Contains constructor, message to pass to exception class for 'what' function, get function for local value exception was thrown at.
	class SubscriptException : public exception {
	public:
		SubscriptException(int subscriptNum) : exception("Subscript exception thrown. Subscript number: ") { subNum = subscriptNum; }
		int getSub() { return subNum; }
	private:
		int subNum;
	};

	//Memory Exception class. Derived from exception class.
	//Contains constructor, message to pass to exception class for 'what' function, get function for local value exception was thrown at.
	class MemoryException : public exception {
	public:
		MemoryException(int newSize) : exception("Memory exception thrown. Array size: ") { sizeNum = newSize; }
		int getSize() { return sizeNum; }
	private:
		int sizeNum;
	};


#endif // !OLSESTEV_EXCEPTION

}