//Steven Olsen
//CS 1415
//April 19, 2016
//Lab 15

/*


*/


//Required Libraries
#include <iostream>
#include <queue>
#include <time.h>
using namespace std;

//Global variables
const int MIN_TIME = 1;
const int MAX_TIME = 4;
	

//Function declarations.
int randTime(); //Returns number 1-4
void outputCustInfo(int, int); //Outputs customer info.
int helpCustomer(int, int &, queue<int> &, int &); //Dequeus and serves customer.






//Purpose: Driver program to simulate a line at a bank using queues.
//Outputs: Messages for all queue operations. Data at end of program.
//Returns: 0 to signify the program is over.
int main() {

	//Amount of minutes to be simulated.
	int sim_time = 480;

	int currentTime = 0;
	
	bool servingCustomer = false;
	
	//Values for customers. arrivalTime is for next customer to arrive.
	//leaveTime is when customer being served will leave.
	//currentCustomer is what is being served at that moment.
	int currentCustomer;
	int arrivalTime;
	int leaveTime;

	//Value for remembering the longest wait a customer experienced.
	int longestWait = 0;
	//Value for largest amount of customers in line.
	int customersInLine = 0;

	//Queue representing line of customers.
	queue<int> customerQueue;


	//Random function will be used, and is seeded with the computer time.
	srand(time(NULL));


	cout << "Enter time you wish to simulate: ";
	cin >> sim_time;



	//First customer served at start of business.
	currentCustomer = randTime(); //Customer arrives at 1-4 minutes.
	arrivalTime = (currentTime + randTime());
	currentTime = arrivalTime; //Time advanced to when customer arrives.
	cout << "Customer Arriving..." << endl; //User shown progress.
	cout << "Serving new customer..." << endl;
	leaveTime = (currentTime + randTime()); //Time calculated when customer will be served.
	servingCustomer = true;


	/*

	LOOPS START HERE
	Each minute is stepped through in order.
	Customer is checked to see if they've been served.
	New customer may arrive.
	If customer served has left, new customer starts being served.

	*/
	for (currentTime; currentTime < sim_time; currentTime++) {

		//TIME FOR CUSTOMER TO LEAVE?
		if (leaveTime == currentTime) {

			outputCustInfo(currentCustomer, currentTime); //Customer info displayed.

			servingCustomer = false; //No customer is being served.
		}

		//NEW CUSTOMER ARRIVING?
		if (arrivalTime == currentTime) {
			cout << "Customer Arriving..." << endl; //Program message.
			customerQueue.push(arrivalTime); //New customer added to queue.
			arrivalTime = (currentTime + randTime()); //New customer time found, which is currentTime plus 1-4 minutes.
			//Line checked to see if it is largest it has been. Largest line size recorded.
			if (customersInLine < customerQueue.size())
				customersInLine = customerQueue.size();
		}

		//CUSTOMER IN LINE?
		if (servingCustomer == false) 
			if (!customerQueue.empty()) { //If queue has customer remaining.
				leaveTime = helpCustomer(currentTime, currentCustomer, customerQueue, longestWait); //Next customer helped.
					servingCustomer = true;
		}


	} //Bank closes for the day.
	cout << "\n\n\n ======= BANK CLOSES FOR THE DAY ======== \n\n\n";




	//Remaining customers served. Much like above loop.
	while (servingCustomer) {

		

		//TIME FOR CUSTOMER TO LEAVE?
		if (leaveTime == currentTime) {

			outputCustInfo(currentCustomer, currentTime); //Customer info displayed.

			servingCustomer = false; //No customer is being served.
		}



		//CUSTOMER IN LINE?
		if (servingCustomer == false)
			if (!customerQueue.empty()) { //If queue has customer remaining.
				leaveTime = helpCustomer(currentTime, currentCustomer, customerQueue, longestWait); //Next customer helped.
				servingCustomer = true;
			}

		currentTime++; //Time incremented.
	}


	//Longest wait and greatest amount of customers in line displayed. Time displayed.
	cout << "Simulation finished. \nLongest wait: " << longestWait << " minutes.\nGreatest number of customers in line: " << customersInLine << " customers.\nTime: " << currentTime << " Minutes";

	return 0;
}





//Purpose: Returns a random time from 1-4
//Returns: 1-4
int randTime() 
{

	return (  rand() % MAX_TIME + MIN_TIME );
}


//Purpose: outputs customer info, called when a customer is served.
//Outputs: Data of time customer arrived and left.
void outputCustInfo(int timeArrived, int timeLeft)
{
	cout << "Customer left. Arrived at " << timeArrived << " minutes and left at " << timeLeft << " minutes." << endl;
}


//Purpose: Dequeues and serves customer. Records longest wait time.
//Receives: Line of customers, previous longest wait, the current time, and the current customer.
//Returns: The time the current customer will leave.
//Notes: Changes the queue, currentCustomer, and longestWait variables, which are all passed by reference.
int helpCustomer(int currentTime, int &currentCustomer, queue<int> &customerLine, int &longestWait)
{
	//Time found customer waited. If it's the longest time, that is recorded.
	if (longestWait < (currentTime - currentCustomer))
		longestWait = (currentTime - currentCustomer);
	cout << "Serving new customer..." << endl;
	currentCustomer = customerLine.front(); //Currentcustomer set to the next one in line.
	customerLine.pop(); //Customer dequeued
	return (currentTime + randTime()); //New leave time found and returned.
	
}
